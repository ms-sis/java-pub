import java.io.*;
import java.util.StringTokenizer;

public class SessionRPLCalculatorStreamCommandHandler extends RPLCalculatorStreamCommandHandler {
    
    private PrintWriter _sessionLogger;

    public SessionRPLCalculatorStreamCommandHandler(InputStream in, OutputStream out, File sessionFile) {
        super(in, out);

        if (sessionFile == null) {
            throw new IllegalArgumentException("Invalid parameters sessionFile > null");
        }

        boolean appendMode = sessionFile.exists();
        try {
            sessionFile.createNewFile();
        } catch (Exception e) {
            throw new RuntimeException("IOException, cannot create sessionFile > "+ sessionFile);
        }

        try {
            _sessionLogger = new PrintWriter( new BufferedWriter ( new FileWriter ( sessionFile, appendMode ) ));
        }
        catch(Exception e) { 
            throw new RuntimeException(e.getMessage());
        }

        if (appendMode) {
            printer().println("[Command History]");
            try {
                BufferedReader history = new BufferedReader( new FileReader(sessionFile) );
                String line;
                while( (line = history.readLine()) != null) {
                    printer().println(line);
                    StringTokenizer commands = parseCommands(line);
                    while(commands.hasMoreTokens()) {
                        applyCommand(
                                commands.nextToken(),
                                false
                        );
                    }
                }
                history.close();
            }
            catch(Exception ignore) {}
        }
    }

    @Override
    public String fetchCommands() {
        String command = super.fetchCommands();
        _sessionLogger.println(command);
        _sessionLogger.flush();
        return command;
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            _sessionLogger.close();
        } catch(Exception ignored) {}
        finally {
            super.finalize();
        }
    }
}
