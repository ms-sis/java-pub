import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class RPLCalculatorLauncher {

    private enum RplCalculatorMode {
        UNASSIGNED,
        LOCAL,
        REMOTE
    }

    private static class RPLCalculatorCommandFactory {
        public static RPLCalculatorStreamCommandHandler Create(InputStream in, OutputStream out, String filePath) {
            if (filePath != null) {
                return Create(in, out, new File(filePath));
            }
            return Create(in, out);
        }

        public static RPLCalculatorStreamCommandHandler Create(InputStream in, OutputStream out, File sessionFile) {
            return new SessionRPLCalculatorStreamCommandHandler(in, out, sessionFile);
        }

        public static RPLCalculatorStreamCommandHandler Create(InputStream in, OutputStream out) {
            return new RPLCalculatorStreamCommandHandler(in, out);
        }
    }

    public static void main(String[] args) {
        if (args.length == 0 || args.length > 2) {
            displayHelp();
            return;
        }    

        RplCalculatorMode mode = RplCalculatorMode.UNASSIGNED;
        String sessionFilePath = null;
        for(int i=0; i<args.length; ++i) {
            switch(args[i]) {
                case "--local":
                    mode = RplCalculatorMode.LOCAL;
                 break;
            case "--network":
                    mode = RplCalculatorMode.REMOTE;
                break;
             default: 
                if (args[i].contains("--session=")) {
                    sessionFilePath = args[i].replace("--session=","");
                }
            }
        }

        try {
            switch (mode) {
                case UNASSIGNED:
                    displayHelp();
                    return;
                case LOCAL:
                    RPLCalculatorCommandFactory
                            .Create(System.in, System.out, sessionFilePath)
                            .run();
                    break;
                case REMOTE:
                    final String sessionFile = sessionFilePath;
                    new Thread() {
                        ServerSocket _listener = new ServerSocket( 1337 );
                        Socket _socket = new Socket();

                        @Override
                        public void start() {
                            System.out.println("Server listen for connection on port 1337");
                            try {
                                _socket = _listener.accept();
                                super.start();
                            } catch (Exception e) {
                                throw new RuntimeException(e.getMessage());
                            }
                        }

                        @Override
                        public  void run() {
                            System.out.println("run");
                            try {
                                RPLCalculatorCommandFactory
                                        .Create(_socket.getInputStream(), _socket.getOutputStream(), sessionFile)
                                        .run();
                            } catch (Exception e) {
                                throw new RuntimeException(e.getMessage());
                            }
                            finally {
                                try {
                                    _socket.close();
                                } catch (Exception e) {
                                    throw new RuntimeException(e.getMessage());
                                }
                            }
                        }
                    }.start();
                    break;
            }
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    private static void displayHelp() {
        System.out.println("[RPL Calculator]");
        System.out.println("--local: Start local RPL Calculator");
        System.out.println("--network: Start network RPL Calculator");
        System.out.println("--session=[file_path]: Set RPL Calculator session file");
    }
}
