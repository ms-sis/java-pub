import java.lang.*;
import java.util.*;

public class RPLObject {
    
    private int _value;

    public RPLObject(int value) {
        _value = value;
    }

    public int value() {
        return _value;
    }

    public RPLObject divide(RPLObject item) {
        if (item == null) { throw  new IllegalArgumentException("Invalid argument item > null"); }
        if (item.value() == 0) {
            throw new RuntimeException("Divide with zero is not permitted");
        }
        return new RPLObject( _value / item.value());
    }

    public RPLObject subtract(RPLObject item) {
        if (item == null) { throw  new IllegalArgumentException("Invalid argument item > null"); }
        return new RPLObject(_value - item.value());
    }

    public RPLObject add(RPLObject item) {
        if (item == null) { throw  new IllegalArgumentException("Invalid argument item > null"); }
        return new RPLObject(_value + item.value());
    }

    public RPLObject multiply(RPLObject item) {
        if (item == null) { throw  new IllegalArgumentException("Invalid argument item > null"); }
        return new RPLObject(_value * item.value());
    }

    @Override
    public String toString() {
        return String.valueOf(_value);
    }
}

