import java.lang.*;
import java.util.*;

public class RPLCalculator {
    private static final int DEFAULT_MAX_STACK_SIZE = 42;
    private List<RPLObject> _stack;
    private int _maxSize;

    public RPLCalculator() {
        this(DEFAULT_MAX_STACK_SIZE);
    }

    public RPLCalculator(int maxSize) {
        if (maxSize < 1) { throw new IllegalArgumentException("Invalid argument maxSize > should be superior or equal to 1"); }

        _stack = new ArrayList<RPLObject>();
        _maxSize = maxSize;
    }

    public boolean push(RPLObject item) {
        if (item == null) { throw  new IllegalArgumentException("Invalid argument item > null"); }
        if (_stack.size() >= _maxSize) { return false; }

        _stack.add(item);
        return true;
    }

    public RPLObject pop() {
        if (_stack.size() == 0) { return null; }
        List<RPLObject> stackCpy = new ArrayList<RPLObject>();
        RPLObject lastElm = null;

        for(int i = 0; i<_stack.size(); ++i) {
            if (i != _stack.size() - 1) {
                stackCpy.add( _stack.get(i) );
            }
            else {
                lastElm = _stack.get(i);   
            }
        }

        _stack = stackCpy;
        return lastElm;
    }

    public void swap() {
        Collections.reverse(_stack);  
    }

    public int size() {
        return _stack.size();
    }

    public void drop() {
        pop();
    }

    public void add() {
        if (_stack.size() >= 2) {
            push(pop().add(pop()));
        }
    }

    public void subtract() {
        if (_stack.size() >= 2) {
            push(pop().subtract(pop()));
        }
    }

    public void divide() {
        if (_stack.size() >= 2) {
            RPLObject leftOperator = pop();
            RPLObject rightOperator = pop();

            if (rightOperator.value() == 0) {
                push(rightOperator);
                push(leftOperator);
                throw new IllegalArgumentException("Divisor is zero");
            }
            push(leftOperator.divide(rightOperator));
        }
    }
    
    public void multiply() {
        if (_stack.size() >= 2) {
            push(pop().multiply(pop()));
        }  
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(_stack.size() * 6);
        for(int i = _stack.size() - 1 ; i>=0; --i) {
            builder.append(i);
            builder.append(" >> ");
            builder.append(_stack.get(i));
            builder.append("\n");
        }
        return builder.toString();
    }
}

