import java.io.*;
import java.util.*;

public class RPLCalculatorStreamCommandHandler {

    private static final String EXIT_CMD = "exit";

    private RPLCalculator _rplCalculator;
    private BufferedReader _in;
    private PrintStream _out;

    protected PrintStream printer() {
        return _out;
    }

    public RPLCalculatorStreamCommandHandler(InputStream in, OutputStream out) {
        if (in == null || out == null) {
            throw new IllegalArgumentException("Invalid parameter > null");
        }

        _rplCalculator = new RPLCalculator();
        try {
            _in = new BufferedReader(new InputStreamReader(in));
            _out = new PrintStream(out);
        } catch (Exception e) {
            throw new RuntimeException("IO stream unreachable > " + e.getMessage());
        }
        displayHelp();
    }

    public void run() {
        StringTokenizer cmds;
        String cmd = "";
        while (!cmd.equals(EXIT_CMD)) {
            cmds = parseCommands(fetchCommands());

            while (cmds.hasMoreTokens()) {
                cmd = cmds.nextToken();
                applyCommand(cmd);
            }
            _out.println(_rplCalculator);
        }
    }

    private void displayHelp() {
        _out.println("Available commands : +,-,*,/,drop,swap,exit,h,help");
        _out.println("Sample: 3 3 +");
    }

    protected String fetchCommands() {
        String command = EXIT_CMD;
        try {
            command = _in.readLine().trim();
        } catch (Exception e) {}
        return command;
    }

    protected StringTokenizer parseCommands(String command) {
        return new StringTokenizer(command);
    }

    protected void applyCommand(String command) {
        applyCommand(command, true);
    }

    protected void applyCommand(String command, boolean verbose) {
        switch (command) {
            case "+":
                _rplCalculator.add();
                break;
            case "-":
                _rplCalculator.subtract();
                break;
            case "/":
                try {
                    _rplCalculator.divide();
                } catch (IllegalArgumentException e) {
                    if (verbose) {
                        _out.println("Divisor is zero");
                    }
                }
                break;
            case "*":
                _rplCalculator.multiply();
                break;
            case "drop":
                _rplCalculator.drop();
            case "swap":
                _rplCalculator.swap();
            case EXIT_CMD:
                break;
            case "h":
            case "help":
                if (verbose) {
                    displayHelp();
                }
                break;
            default:
                try {
                    Integer value = Integer.parseInt(command);
                    if (!_rplCalculator.push(new RPLObject(value))) {
                        if (verbose) {
                            _out.println("The stack is full!");
                        }
                    }
                } catch (Exception e) {
                    if (verbose) {
                        _out.println("Invalid command > " + command);
                    }
                }
                break;
        }
    }

    @Override
    protected void finalize() throws Throwable {
        try {
            _in.close();
            _out.close();
        } catch (Exception e) {
        } finally {
            super.finalize();
        }
    }
}
