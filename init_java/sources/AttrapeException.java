public class AttrapeException {


	static int average( String[] in ) {
		if (in == null) {
            throw new RuntimeException("In parameter should not be null");
        }
 
        if (in.length == 0) {
            return 0;
        }

        int sum=0;
        for( int i=0; i < in.length; i++ ) 
        try {
			sum += Integer.parseInt( in[i] );
		} catch( NumberFormatException e ) {
			System.out.println( "Nop" );
		}

		return sum/in.length;
	}


	public static void main( String[] args ) {
		System.out.println( "The average is " + average( args ) );
	}

}
