import java.io.*;
import java.util.*;

public class HelloThread {

    public static void main(String[] args) {
               
        Thread looperA = new Thread() {
            public void run() {
                while(true) {
                    System.out.print("a");
                }
            }
        };

        Thread looperB = new Thread() {
            public void run() {
                while(true) {
                    System.out.print("b");
                }
            }
        };

        looperA.start();
        looperB.start();
    }
}
