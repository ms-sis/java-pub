package nementon.io;

import nementon.io.packets.Packet;
import nementon.io.packets.visitor.PacketFilters;
import nementon.io.packets.visitor.PrintVisitor;
import nementon.io.packets.visitor.SearchPacketVisitor;
import nementon.io.pcap.PcapReader;

public class Main {
    public static void main(String[] args) {

        String linkLayer = null, internetLayer = null, transportLayer = null, applicationLayer = null;
        if (args.length > 0) {
            for(String arg : args) {
                if (arg.contains("--link=")) {
                    linkLayer = arg.replace("--link=", "");
                }
                if (arg.contains("--internet=")) {
                    internetLayer = arg.replace("--internet=", "");
                }
                if (arg.contains("--transport=")) {
                    transportLayer = arg.replace("--transport=", "");
                }
                if (arg.contains("--application=")) {
                    applicationLayer = arg.replace("--application=", "");
                }
                if (arg.equalsIgnoreCase("-h") || arg.equalsIgnoreCase("--help")) {
                    displayHelp();
                }
            }
        }
        else {
            displayHelp();
            return;
        }

        SearchPacketVisitor searchVisitor = new SearchPacketVisitor(
                new PacketFilters(linkLayer, internetLayer, transportLayer, applicationLayer));
        try {
            int count = 0;
            PcapReader reader = new PcapReader(args[0]);
            while(reader.hasNext()) {
                reader.next().accept(searchVisitor);
                count++;
            }

            PrintVisitor printVisitor = new PrintVisitor();
            for(Packet packet : searchVisitor.matchedPackets()) {
                packet.accept(printVisitor);
            }
            System.out.println(String.format("\n\nFound %s match packet(s) on %s packet(s) !", searchVisitor.matchedPackets().size(), count));
        }
        catch(Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private static void displayHelp() {
        System.out.println("PcapFilePath [options]");
        System.out.println("--link=[String]       \t\t Add filter on link layer");
        System.out.println("--internet=[String]   \t\t Add filter on internet layer");
        System.out.println("--transport=[String]  \t\t Add filter on transport layer");
        System.out.println("--application[String] \t\t Add filter on application layer");
        System.out.println("-h --help             \t\t Display this help");
    }
}
