package nementon.io.pcap;


// http://www.tcpdump.org/linktypes.html
public enum PcapNetworkLinkLayer {
    UNDEFINED(-1),
    NULL(0),
    ETHERNET(1),
    AX253(3),
    IEEE802_5(6),
    ARCNET_BSD(7),
    SLIP(8),
    PPP(9),
    FDDI(10),
    PPP_HDLC(50),
    PPP_ETHER(51),
    ATM_RFC1483(100),
    RAW(101),
    C_HDLC(104),
    IEEE02_11(105),
    FRELAYY(107),
    LOOP(108),
    LINUX_SLL(113),
    LTALK(114),
    PFLOG(117),
    IEEE802_11_PRISM(119),
    IP_OVER_FC(122),
    SUNATM(123),
    IEEE802_11_RADIOTAP(127),
    ARCNET_LINUX(129),
    APPLE_IP_OVER_IEEE1394(138),
    MTP2_WITH_PHDR(139),
    MTP2(140),
    MTP3(141),
    SCCP(142),
    DOCSIS(143),
    LINUX_IRDA(144);
    /**
     * Etc ....
     */

    private final long _value;
    PcapNetworkLinkLayer(long value) {
        _value = value;
    }

    public static PcapNetworkLinkLayer getEnumFromValue(long value) {
        for(PcapNetworkLinkLayer layer : PcapNetworkLinkLayer.values()) {
            if (layer.value() == value) {
                return layer;
            }
        }
        return PcapNetworkLinkLayer.UNDEFINED;
    }

    public long value() {
        return _value;
    }
}
