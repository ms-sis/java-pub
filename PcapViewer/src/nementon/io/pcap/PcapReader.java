package nementon.io.pcap;

import nementon.io.packets.Packet;
import nementon.io.packets.PacketFactory;
import nementon.io.packets.PacketHeader;
import nementon.io.packets.layers.link.EthernetPacket;
import nementon.io.packets.layers.link.EthernetPacketType;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;

public class PcapReader implements Iterator<Packet> {

    private PcapHeader _globalHeader;
    private DataInputStream _in;
    private boolean _nextItemConsumed = true;
    private boolean _haveNextItem = false;
    private Packet _nextItem;

    public PcapReader(String pcapFilePath) throws FileNotFoundException {
        this(new File(pcapFilePath));
    }

    public PcapReader(File pcapFile) throws FileNotFoundException {
        _in = new DataInputStream( new FileInputStream(pcapFile));

        byte[] globalHeaderData = new byte[PcapHeader.HEADER_SIZE];
        if (readByte(_in, globalHeaderData)) {
            throw new RuntimeException("Invalid file format, can not read PCAP header");
        }
        _globalHeader = new PcapHeader(globalHeaderData);
    }

    private Packet readNextPacket(DataInputStream in) {
        byte[] packetHeaderData = new byte[PacketHeader.HEADER_SIZE];

        if (readByte(in, packetHeaderData)) {
            return null;
        }

        PacketHeader packetHeader = new PacketHeader(_globalHeader, packetHeaderData);
        byte[] packetData = new byte[(int)packetHeader.originLength()];
        if (readByte(in, packetData)) {
            return null;
        }

        return PacketFactory.Create(_globalHeader.linkLayer(), packetData);
    }

    protected boolean readByte(DataInputStream in, byte[] buffer) {
        try {
            in.readFully(buffer);
            return false;
        }
        catch(Exception e){
            // End of file, ignore it
        }
        return true;
    }

     @Override
    public boolean hasNext() {
        if (!_nextItemConsumed) {
            return _haveNextItem;
        }
        else {
            _nextItem = readNextPacket(_in);
            _nextItemConsumed = false;
            _haveNextItem = _nextItem != null;
            return _haveNextItem;
        }
    }

    @Override
    public Packet next() {
        if (hasNext()) {
            _nextItemConsumed = true;
        }
        return _nextItem;
    }

    @Override
    public void finalize() throws Throwable {
        try {
            _in.close();
        }
        catch (Exception ignored) {}
        finally {
            super.finalize();
        }
    }
}
