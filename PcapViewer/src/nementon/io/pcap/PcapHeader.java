package nementon.io.pcap;

import nementon.io.packets.utils.BytesUtils;

import java.nio.ByteOrder;
import java.util.Arrays;

public class PcapHeader {

    public static final int HEADER_SIZE = 24;
    private static final byte[] MAGIC_BIG_ENDIAN = { (byte) 0xa1, (byte) 0xb2, (byte) 0xc3, (byte) 0xd4 };
    private static final byte[] MAGIC_LITTLE_ENDIAN = { (byte) 0xd4, (byte) 0xc3, (byte) 0xb2, (byte) 0xa1 };

    /*
        unsigned int 32
        magic_number: used to detect the file format itself and the byte ordering.
        The writing application writes 0xa1b2c3d4 with it's native byte ordering format into this field.
        The reading application will read either 0xa1b2c3d4 (identical) or 0xd4c3b2a1 (swapped).
        If the reading application reads the swapped 0xd4c3b2a1 value, it knows that all the following fields will have to be swapped too.
        For nanosecond-resolution files, the writing application writes 0xa1b23c4d,
        with the two nibbles of the two lower-order bytes swapped,
        and the reading application will read either 0xa1b23c4d (identical) or 0x4d3cb2a1 (swapped).
     */

    /* unsigned int 16
       version major
     */

    /*
        unsigned int 16
        version minor
     */

    /*
        unsigned int 32
        thiszone: the correction time in seconds between GMT (UTC) and the local timezone of the following packet header timestamps.
        Examples: If the timestamps are in GMT (UTC), thiszone is simply 0. If the timestamps are in Central European time (Amsterdam, Berlin, ...)
        which is GMT + 1:00, thiszone must be -3600. In practice, time stamps are always in GMT, so thiszone is always 0.
     */

    /*
        unsigned int 32
        sigfigs: in theory, the accuracy of time stamps in the capture; in practice, all tools set it to 0
     */

    /*
        unsigned int 32
        snaplen: the "snapshot length" for the capture (typically 65535 or even more, but might be limited by the user),
     */

    /*
        unsigned int 32
        network: link-layer header type, specifying the type of headers at the beginning of the packet (e.g. 1 for Ethernet,
        see tcpdump.org's link-layer header types page for details); this can be various types such as 802.11, 802.11
        with various radio information, PPP, Token Ring, FDDI, etc.
     */


    private byte[] _header;
    private ByteOrder _byteOrder;

    public PcapHeader(byte[] header) {
        if (header.length != HEADER_SIZE) {
            throw new RuntimeException("InvalidArgumentException: invalid header");
        }
        _header = header;
        _byteOrder = extractByteOrder();
    }

    private ByteOrder extractByteOrder() {
        byte[] magicNumber = Arrays.copyOfRange(_header, 0, MAGIC_BIG_ENDIAN.length);

        if (Arrays.equals(MAGIC_BIG_ENDIAN, magicNumber)) {
            return ByteOrder.BIG_ENDIAN;
        }
        else if (Arrays.equals(MAGIC_LITTLE_ENDIAN, magicNumber)) {
            return ByteOrder.LITTLE_ENDIAN;
        }

        throw new RuntimeException("Can not determine byte order, invalid pcap magic number");
    }

    public ByteOrder byteOrder() {
        return _byteOrder;
    }

    public int versionMajor() {
        return  BytesUtils.extractUnsignedShort(4, _header, _byteOrder);
    }

    public int versionMinor() {
        return BytesUtils.extractUnsignedShort(7, _header, _byteOrder);
    }

    public long thiszone() {
        return BytesUtils.extractUnsignedInt(8, _header, _byteOrder);
    }

    public long sigfigs() {
        return BytesUtils.extractUnsignedInt(12, _header, _byteOrder);
    }

    public long snaplen() {
        return BytesUtils.extractUnsignedInt(16, _header, _byteOrder);
    }

    public PcapNetworkLinkLayer linkLayer() {
        long linkLayerTypeValue = BytesUtils.extractUnsignedInt(20, _header, _byteOrder);
        return PcapNetworkLinkLayer.getEnumFromValue(linkLayerTypeValue);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(256);

        builder.append(String.format("Byte order : %s \n ", byteOrder()));
        builder.append(String.format("Version : %d.%d \n", versionMajor(), versionMinor()));
        builder.append(String.format("This zone : %d \n", thiszone()));
        builder.append(String.format("Sigfigs : %d \n", sigfigs()));
        builder.append(String.format("Snaplen : %d \n", snaplen()));
        builder.append(String.format("Link layer : %s \n", linkLayer()));

        return builder.toString();
    }
}
