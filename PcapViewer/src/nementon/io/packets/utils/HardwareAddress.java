package nementon.io.packets.utils;

import java.util.Arrays;

public class HardwareAddress {
    private static final int HwAddressLength = 6;

    private byte[] _hwAddress;

    public HardwareAddress(byte[] hwAddress) {
        if (hwAddress.length != HwAddressLength) {
            throw new IllegalArgumentException("Invalid hardware address, it should be 6 bytes length"); }

        _hwAddress = Arrays.copyOfRange(hwAddress, 0, hwAddress.length);
    }

    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder(17);
        for (int i=0; i<HwAddressLength; ++i) {
            builder.append(
                    String.format("%02X%s", _hwAddress[i], (i < HwAddressLength - 1) ? "-" : ""));
        }
        return builder.toString();
    }
}
