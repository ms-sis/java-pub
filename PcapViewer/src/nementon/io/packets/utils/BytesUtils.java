package nementon.io.packets.utils;

import java.nio.ByteOrder;

public class BytesUtils {
    public static int extractUnsignedShort(int offset, byte[] buffer) {
        return extractUnsignedShort(offset, buffer, ByteOrder.BIG_ENDIAN);
    }

    public static int extractUnsignedShort(int offset, byte[] buffer, ByteOrder byteOrder) {
        if (offset + 1 > buffer.length) { throw new IndexOutOfBoundsException("Can not read unsigned short"); }

        if (byteOrder == ByteOrder.BIG_ENDIAN) {
            return (buffer[offset] & 0xFF) << 8 | buffer[offset + 1] & 0xFF;
        }
        return (buffer[offset + 1] & 0xFF) << 8 | buffer[offset] & 0xFF;

    }

    public static long extractUnsignedInt(int offset, byte[] buffer) {
        return extractUnsignedInt(offset, buffer, ByteOrder.BIG_ENDIAN);
    }

    public static long extractUnsignedInt(int offset, byte[] buffer, ByteOrder byteOrder) {
        if (offset + 3 > buffer.length) {
            throw new IndexOutOfBoundsException("Can not read unsigned int");
        }
        /*
        ByteBuffer intBuffer = ByteBuffer.allocate(4);
        for(int i=0; i<4; ++i) {
            intBuffer.put(buffer[offset + i]);
        }
        intBuffer.order(byteOrder);
        return intBuffer.getInt(0) & 0xFFFFFFFFL;
        */
       if (byteOrder == ByteOrder.BIG_ENDIAN) {
            return ((buffer[offset] & 0xff) << 24 | (buffer[offset + 1] & 0xff) << 16
                    | (buffer[offset + 2] & 0xff) << 8 | buffer[offset + 3] & 0xff) & 0xFFFFFFFFL;
        }

        return ((buffer[offset + 3] & 0xff) << 24 | (buffer[offset + 2] & 0xff) << 16
                | (buffer[offset + 1] & 0xff) << 8 | buffer[offset] & 0xff) & 0xFFFFFFFFL;
    }
}

