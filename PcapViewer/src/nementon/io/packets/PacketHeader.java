package nementon.io.packets;


import nementon.io.packets.utils.BytesUtils;
import nementon.io.pcap.PcapHeader;

public class PacketHeader {
    public static final int HEADER_SIZE = 16;

    /*
    unsigned int 32
    ts_sec: the date and time when this packet was captured. This value is in seconds since
    January 1, 1970 00:00:00 GMT; this is also known as a UN*X time_t.
    You can use the ANSI C time() function from time.h to get this value, but you might use a more optimized way
    to get this timestamp value. If this timestamp isn't based on GMT (UTC), use thiszone from the global header for adjustments.
     */

    /*
    unsigned int 32
    ts_usec: in regular pcap files, the microseconds when this packet was captured, as an offset to ts_sec.
    In nanosecond-resolution files, this is, instead, the nanoseconds when the packet was captured,
     as an offset to ts_sec /!\ Beware: this value shouldn't reach 1 second
     (in regular pcap files 1 000 000; in nanosecond-resolution files, 1 000 000 000); in this case ts_sec must be increased instead!
     */

    /*
    unsigned int 32
    incl_len: the number of bytes of packet data actually captured and saved in the file.
    This value should never become larger than orig_len or the snaplen value of the global header.
     */

    /*
    unsigned int 32
    orig_len: the length of the packet as it appeared on the network when it was captured.
    If incl_len and orig_len differ, the actually saved packet size was limited by snaplen.
     */

    private byte[] _header;
    private PcapHeader _context;

    public PacketHeader(PcapHeader context, byte[] header) {
        if (context == null) {
            throw new IllegalArgumentException("Context can not be null !");
        }
        // TODO improve header validation
        if (header.length != HEADER_SIZE) {
            throw new RuntimeException("InvalidArgumentException: Invalid packet header");
        }

        _header = header;
        _context = context;
    }

    public long timestampSecond() {
        return BytesUtils.extractUnsignedInt(0, _header, _context.byteOrder());
    }

    public long timestampMicroSecond() {
        return BytesUtils.extractUnsignedInt(4, _header, _context.byteOrder());
    }

    public long includeLength() {
        return BytesUtils.extractUnsignedInt(8, _header, _context.byteOrder());
    }

    public long originLength() {
        return BytesUtils.extractUnsignedInt(12, _header, _context.byteOrder());
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(250);

        builder.append(String.format("Timestamp (seconds) : %d \n", timestampSecond()));
        builder.append(String.format("Timestamp (u-seconds) : %d \n", timestampMicroSecond()));
        builder.append(String.format("Include length : %d \n", includeLength()));
        builder.append(String.format("Original length : %d \n", originLength()));

        return builder.toString();
    }
}
