package nementon.io.packets;

import nementon.io.packets.layers.application.ApplicationPacket;
import nementon.io.packets.layers.application.DnsPacket;
import nementon.io.packets.layers.application.FtpPacket;
import nementon.io.packets.layers.application.HttpPacket;
import nementon.io.packets.layers.internet.*;
import nementon.io.packets.layers.internet.ip.IpProtocolType;
import nementon.io.packets.layers.internet.ip.OfficialTcpPort;
import nementon.io.packets.layers.internet.ip.OfficialUdpPort;
import nementon.io.packets.layers.link.EthernetPacket;
import nementon.io.packets.layers.link.EthernetPacketType;
import nementon.io.packets.layers.link.LinkPacket;
import nementon.io.packets.layers.transport.TcpPacket;
import nementon.io.packets.layers.transport.TransportPacket;
import nementon.io.packets.layers.transport.UdpPacket;
import nementon.io.pcap.PcapNetworkLinkLayer;


public class PacketFactory {
    public static  Packet Create(PcapNetworkLinkLayer link, byte[] packetData) {
        switch (link)
        {
            case ETHERNET:
                return new EthernetPacket(packetData);
            default:
                return new LinkPacket(packetData);
        }
    }

    public static Packet Create(EthernetPacketType type, byte[] packetData) {
        switch (type) {
            case IPV4:
                return new Ipv4Packet(packetData);
            case IPV6:
                return new Ipv6Packet(packetData);
            case ARP:
                return new ArpPacket(packetData);
            default:
                return new InternetPacket(packetData);
        }
    }

    public static Packet Create(IpProtocolType protocol, byte[] packetData) {
        switch (protocol) {
            case TCP:
                return new TcpPacket(packetData);
            case UDP:
                return new UdpPacket(packetData);
            case ICMP:
                return new IcmpPacket(packetData);
            default:
                return new TransportPacket(packetData);
        }
    }

    public static Packet Create(TcpPacket tcpPacket, byte[] packetData) {
        OfficialTcpPort port = OfficialTcpPort.getEnumFormValue(tcpPacket.destinationPort());
        switch (port) {
            case DNS:
                return new DnsPacket(packetData);
            case HTTP:
                return new HttpPacket(packetData);
            case FTP:
                return new FtpPacket(packetData);
        }

        port = OfficialTcpPort.getEnumFormValue(tcpPacket.sourcePort());
        switch (port) {
            case DNS:
                return new DnsPacket(packetData);
            case HTTP:
                return new HttpPacket(packetData);
            case FTP:
                return new FtpPacket(packetData);
        }

        return new ApplicationPacket(packetData);
    }

    public static Packet Create(UdpPacket udpPacket, byte[] packetData) {
        OfficialUdpPort port = OfficialUdpPort.getEnumFromValue(udpPacket.destinationPort());
        switch (port) {
            case DNS:
                return new DnsPacket(packetData);
            case FTP:
                return new FtpPacket(packetData);
            case HTTP:
                return new HttpPacket(packetData);
        }

        port = OfficialUdpPort.getEnumFromValue(udpPacket.sourcePort());
        switch (port) {
            case DNS:
                return new DnsPacket(packetData);
            case FTP:
                return new FtpPacket(packetData);
            case HTTP:
                return new HttpPacket(packetData);
        }

        return new ApplicationPacket(packetData);
    }

}
