package nementon.io.packets.layers.application;

import nementon.io.packets.visitor.IPacketVisitor;

public class FtpPacket extends ApplicationPacket {
    public FtpPacket(byte[] data) {
        header(data);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(header().length);

        builder.append("----------------- FTP ------------------ \n");
        builder.append(new String(header()));

        return builder.toString();
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
