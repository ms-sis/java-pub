package nementon.io.packets.layers.application;

import nementon.io.packets.layers.application.dns.DnsRecordAnswer;
import nementon.io.packets.layers.application.dns.DnsRecordQuery;
import nementon.io.packets.utils.BytesUtils;
import nementon.io.packets.visitor.IPacketVisitor;

import java.nio.ByteBuffer;
import java.util.*;

// http://tools.ietf.org/html/rfc1035
public class DnsPacket extends ApplicationPacket {
    static class DnsField {
        static final int HeaderSize = 12;
        static final int TransactionIdPosition = 0;
        static final int FlagsPosition = TransactionIdPosition + 2;
        static final int QuestionCountPosition = FlagsPosition + 2;
        static final int AnswerCountPosition = QuestionCountPosition + 2;
        static final int NameServerCount = AnswerCountPosition + 2;
        static final int AdditionSectionCount = NameServerCount + 2;
    }

    static class DnsFlagMask {
        static final int QR = 0x01;
        static final int QUERY = 0x01;
        static final int IQUERY = 0x01;
        static final int STATUS = 0x01;
        static final int AA = 0x01;
        static final int TC = 0x11;
        static final int RD = 0x11;
        static final int RA = 0x11;
        static final int R_NO = 0x11;
        static final int R_FORMAT = 0x11;
        static final int R_SERVER_FAILURE = 0x11;
        static final int R_NAME_ERROR = 0x11;
        static final int R_NOT_IMPLEMENTED = 0x11;
        static final int R_REFUSED = 0x11;
    }

    enum DomainNameFormat {
        LABEL,
        POINTER
    }

    private List<DnsRecordQuery> _queries;
    private List<DnsRecordAnswer> _answers;
    private byte[] _rawData;

    public DnsPacket(byte[] data) {
        if (data.length < DnsField.HeaderSize) {
            throw new RuntimeException("Invalid Dns packet, header size should be 4bytes (32bits) > "+ data.length);
        }

        _rawData = data;
        _queries = new ArrayList<>();
        _answers = new ArrayList<>();

        header(
                Arrays.copyOfRange(
                    data, 0, DnsField.HeaderSize)
        );

        ByteBuffer dataBuffer = ByteBuffer.wrap(Arrays.copyOfRange(data, DnsField.HeaderSize, data.length));
        extractQueries(dataBuffer);
        extractAnswers(dataBuffer);
    }

    private void extractQueries(ByteBuffer queriesBuffer) {
        for (int i = 0; i < queryCount(); ++i) {

            byte[] queryData = new byte[4];
            List<String> names =  extractDomainNamesAndMoveBuffer(queriesBuffer);

            queriesBuffer.get(queryData);
            _queries.add(new DnsRecordQuery(names, queryData));
        }
    }

    private void extractAnswers(ByteBuffer answersBuffer) {
        for(int i = 0; i < answerCount(); ++i) {

            List<String> names = extractDomainNamesAndMoveBuffer(answersBuffer);

            byte[] answerData = new byte[10];
            answersBuffer.get(answerData);

            int rdataLength = BytesUtils.extractUnsignedShort(8, answerData);
            byte[] rData = new byte[rdataLength];
            answersBuffer.get(rData);

            _answers.add(new DnsRecordAnswer(names, answerData, rData));
        }
    }

    List<String> extractDomainNamesAndMoveBuffer(ByteBuffer buffer) {
        List<String> names = new ArrayList<>();

        int format = (buffer.get() & 0xFF);
        if (extractNameFormat(format) == DomainNameFormat.LABEL) {
            int nameLength = format;
            while (nameLength != 0) {
                byte[] nameContainer = new byte[nameLength];
                for(int j = 0; j < nameLength; ++j) {
                    nameContainer[j] = buffer.get();
                }
                names.add(new String(nameContainer));
                nameLength = (buffer.get() & 0xFF);
                if (extractNameFormat(nameLength) == DomainNameFormat.POINTER) {
                    int pointerOffset = buffer.get();
                    names.addAll(extractDomainNamesAndMoveBuffer(
                            ByteBuffer.wrap(_rawData, pointerOffset, _rawData.length - pointerOffset)
                    ));
                    break;
                }
            }
        }
        else if (extractNameFormat(format) == DomainNameFormat.POINTER) {
            int pointerOffset = buffer.get();
            names.addAll(extractDomainNamesAndMoveBuffer(
                    ByteBuffer.wrap(_rawData, pointerOffset, _rawData.length - pointerOffset)
            ));
        }

        return names;
    }

    private DomainNameFormat extractNameFormat(int val) {
        if ((val & (1<<7)) != 0 && (val & (1<<6)) != 0) {
            return DomainNameFormat.POINTER;
        }
        else if ((val & (1<<7)) == 0 && (val & (1 << 6)) == 0) {
            return DomainNameFormat.LABEL;
        }
        throw new RuntimeException("Unknown domain name format, neither a label or a pointer");
    }

    public int transactionId() {
        return BytesUtils.extractUnsignedShort(
                DnsField.TransactionIdPosition,
                header()
        );
    }

    public boolean QR() {
        return (flags()[0] & DnsFlagMask.QR) != 0;
    }

    public boolean QUERY(){
        return (flags()[0] & DnsFlagMask.QUERY) != 0;
    }

    public boolean IQUERY() {
        return (flags()[0] & DnsFlagMask.IQUERY) != 0;
    }

    public boolean STATUS() {
        return (flags()[0] & DnsFlagMask.STATUS) != 0;
    }

    public boolean AA() {
        return (flags()[0] & DnsFlagMask.AA) != 0;
    }

    public boolean TC() {
        return (flags()[0] & DnsFlagMask.TC) != 0;
    }

    public boolean RD() {
        return (flags()[0] & DnsFlagMask.RD) != 0;
    }

    public boolean RA() {
        return (flags()[1] & DnsFlagMask.RA) != 0;
    }

    int flagsWeight() {
        return BytesUtils.extractUnsignedShort(
                DnsField.FlagsPosition,
                header()
        );
    }

    public byte[] flags() {
        return Arrays.copyOfRange(
                header(),
                DnsField.FlagsPosition,
                DnsField.FlagsPosition + 2);
    }

    public int queryCount() {
        return BytesUtils.extractUnsignedShort(
                DnsField.QuestionCountPosition,
                header()
        );
    }

    public int answerCount() {
        return BytesUtils.extractUnsignedShort(
                DnsField.AnswerCountPosition,
                header()
        );
    }

    public int nameServerCount() {
        return BytesUtils.extractUnsignedShort(
                DnsField.NameServerCount,
                header()
        );
    }

    public int additionalSectionCount() {
        return BytesUtils.extractUnsignedShort(
                DnsField.AdditionSectionCount,
                header()
        );
    }

    public List<DnsRecordQuery> queries() {
        return Collections.unmodifiableList(_queries);
    }

    public List<DnsRecordAnswer> answers() {
        return Collections.unmodifiableList(_answers);
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(header().length * 2);

        builder.append(String.format("Transaction ID : 0x%04x (%d) \n", transactionId(), transactionId()));
        builder.append(String.format("Flags : 0x%04x \n", flagsWeight()));
        builder.append(String.format("Query count : %d \n", queryCount()));
        builder.append(String.format("Answer count : %d \n", answerCount()));
        builder.append(String.format("Name server count : %d \n", nameServerCount()));
        builder.append(String.format("Addition section count : %d \n", additionalSectionCount()));

        if (queryCount() > 0) {
            builder.append("Queries : \n");
            for (DnsRecordQuery query : queries()) {
                builder.append("\t");
                builder.append(query);
            }
        }

        if (answerCount() > 0) {
            builder.append("Answers : \n");
            for(DnsRecordAnswer answer : answers()) {
                builder.append("\t");
                builder.append(answer);
            }
        }

        return builder.toString();
    }
}
