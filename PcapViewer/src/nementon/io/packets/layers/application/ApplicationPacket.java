package nementon.io.packets.layers.application;

import nementon.io.packets.Packet;
import nementon.io.packets.visitor.IPacketVisitor;

public class ApplicationPacket extends Packet {
    public ApplicationPacket() {
        super();
    }

    public ApplicationPacket(byte[] data) {
        header(data);
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}