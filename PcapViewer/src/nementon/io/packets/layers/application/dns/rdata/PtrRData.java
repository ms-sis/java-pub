package nementon.io.packets.layers.application.dns.rdata;

import nementon.io.packets.layers.application.dns.DnsRecordAnswer;

public class PtrRData extends RData {

    public PtrRData(DnsRecordAnswer owner, byte[] data) {
        super(owner);
    }

    public String ipAddress() {
        return "TODO / handle that ";
    }

    @Override
    public String toString() {
        return  "Supplied ip address : " + ipAddress();
    }
}
