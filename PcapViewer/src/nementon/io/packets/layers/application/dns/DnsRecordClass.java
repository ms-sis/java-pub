package nementon.io.packets.layers.application.dns;

public enum DnsRecordClass {
    UNKNOWN(-1),
    INTERNET(1),
    CHAOS(3),
    HESIOD(4),
    NONE(254),
    ANY(255);

    private int _value;
    DnsRecordClass(int value) {
        _value = value;
    }

    public static DnsRecordClass getEnumFromValue(int value) {
        for(DnsRecordClass qclass : DnsRecordClass.values()) {
            if (qclass.value() == value) {
                return qclass;
            }
        }
        return DnsRecordClass.UNKNOWN;
    }

    public int value() {
        return _value;
    }
}
