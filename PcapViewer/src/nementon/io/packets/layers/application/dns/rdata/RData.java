package nementon.io.packets.layers.application.dns.rdata;

import nementon.io.packets.layers.application.dns.DnsRecordAnswer;

public abstract class RData {

    DnsRecordAnswer _owner;

    public RData(DnsRecordAnswer owner) {
        if (owner == null ) {
            throw new IllegalArgumentException("DnsRecordAnswer owner should not be null !");
        }

        _owner = owner;
    }

    protected DnsRecordAnswer owner() {
        return _owner;
    }
}
