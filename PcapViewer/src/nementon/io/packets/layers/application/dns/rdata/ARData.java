package nementon.io.packets.layers.application.dns.rdata;

import nementon.io.packets.layers.application.dns.DnsRecordAnswer;

import java.net.InetAddress;

public class ARData extends RData {
    private byte[] _data;

    public ARData(DnsRecordAnswer owner, byte[] data) {
        super(owner);

        if (data.length != 4) {
            throw new RuntimeException("Invalid A.RData, should be an unsigned 32bit values (4bytes) length > " + data.length);
        }

        _data = data;
    }

    InetAddress address() {
        try {
            return InetAddress.getByAddress(_data);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        return "Address : " + address();
    }
}
