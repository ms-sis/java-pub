package nementon.io.packets.layers.application.dns;

import java.util.List;

public class DnsRecordQuery extends DnsRecord {
    public DnsRecordQuery(List<String> names, byte[] data) {
        super(names, data);
    }
}
