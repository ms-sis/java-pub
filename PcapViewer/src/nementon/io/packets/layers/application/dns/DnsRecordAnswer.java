package nementon.io.packets.layers.application.dns;

import nementon.io.packets.layers.application.dns.rdata.*;
import nementon.io.packets.utils.BytesUtils;

import java.util.List;

public class DnsRecordAnswer extends DnsRecord {

    private long _ttl;
    private int _rdLength;
    private RData _rData;

    public DnsRecordAnswer(List<String> names, byte[] data, byte[] rData) {
        super(names, data);

        _ttl = BytesUtils.extractUnsignedInt(4,data);
        _rdLength = BytesUtils.extractUnsignedShort(8, data);

        switch (type()) {
            case SOA:
                _rData = new SoaRData(this, rData);
                break;
            case MX:
                _rData = new MxRData(this, rData);
                break;
            case A:
                _rData = new ARData(this, rData);
                break;
            case PTR:
                _rData = new PtrRData(this, rData);
                break;
            case NS:
                _rData = new NsRData(this, rData);
                break;
        }

    }

    public long timeToLive() {
        return _ttl;
    }

    public int dataLength() {
        return _rdLength;
    }

    public RData rData() {
        return _rData;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(128);

        builder.append(super.toString());
        builder.append("\t\tTTL : ");
        builder.append(timeToLive());
        builder.append("\n\t\tData length : ");
        builder.append(dataLength());
        builder.append("\n");
        if (rData() != null) {
            builder.append("\t\t");
            builder.append(rData());
        }

        return builder.toString();
    }
}
