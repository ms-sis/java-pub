package nementon.io.packets.layers.application.dns;

public enum DnsRecordType {
    UNKNOWN(-1),
    A(1),
    NS(2),
    CNAME(5),
    SOA(6),
    WKS(11),
    PTR(12),
    MX(15),
    SRV(33),
    A6(38),
    ANY(255);

    private int _value;
    DnsRecordType(int value) {
        _value = value;
    }

    public static DnsRecordType getEnumFromValue(int value) {
        for(DnsRecordType type : DnsRecordType.values()) {
            if (type.value() == value) {
                return type;
            }
        }
        return DnsRecordType.UNKNOWN;
    }

    public int value() {
        return _value;
    }
}
