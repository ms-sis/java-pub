package nementon.io.packets.layers.application.dns.rdata;

import nementon.io.packets.layers.application.dns.DnsRecordAnswer;

public class NsRData extends RData {

    public NsRData(DnsRecordAnswer owner, byte[] data) {
        super(owner);
    }

    public String domainNameServer() {
        return "TODO / handle that ";
    }

    @Override
    public String toString() {
        return "Domain NS : " + domainNameServer();
    }
}
