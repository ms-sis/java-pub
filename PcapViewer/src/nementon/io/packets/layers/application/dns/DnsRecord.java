package nementon.io.packets.layers.application.dns;

import nementon.io.packets.utils.BytesUtils;

import java.util.List;

public abstract class DnsRecord {

    private List<String> _names;
    private DnsRecordType _type;
    private DnsRecordClass _class;

    public DnsRecord(List<String> names, byte[] data) {
        if (data.length < 4) {
            throw new IllegalArgumentException("Question data array size should be at least 4");
        }

        _names = names;
        _type = DnsRecordType.getEnumFromValue(
                BytesUtils.extractUnsignedShort(0, data));
        _class = DnsRecordClass.getEnumFromValue(
                BytesUtils.extractUnsignedShort(2, data));
    }

    public String name() {
        StringBuilder builder = new StringBuilder(48);

        for (int i = 0; i < _names.size(); ++i) {
            builder.append(_names.get(i));

            if (i + 1 != _names.size()) {
                builder.append(".");
            }
        }

        return builder.toString();
    }

    public DnsRecordType type() {
        return _type;
    }

    public DnsRecordClass dnsClass() {
        return _class;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(name());
        builder.append(": type ");
        builder.append(type());
        builder.append(", class ");
        builder.append(dnsClass());
        builder.append("\n");

        return builder.toString();
    }
}
