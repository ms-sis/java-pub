package nementon.io.packets.layers.application.dns.rdata;

import nementon.io.packets.layers.application.dns.DnsRecordAnswer;
import nementon.io.packets.utils.BytesUtils;

public class SoaRData extends RData {

    private byte[] _data;

    public SoaRData(DnsRecordAnswer owner, byte[] data) {
        super(owner);

        if (data.length < 20) {
            throw new RuntimeException("Invalid data, it size should be at least 20bytes (160bits) > " + data.length);
        }

        _data = data;
    }

    public String primaryNameServer() {
        return "TODO / handle that";
    }

    public String adminMailBox() {
        return "TODO / handle that";
    }

    public long serialNumber() {
        return BytesUtils.extractUnsignedInt(_data.length - 20, _data);
    }

    public long refreshInterval() {
        return BytesUtils.extractUnsignedInt(_data.length - 16, _data);
    }

    public long retryInterval() {
        return BytesUtils.extractUnsignedInt(_data.length - 12, _data);
    }

    public long expirationLimit() {
        return BytesUtils.extractUnsignedInt(_data.length - 8, _data);
    }

    public long minimumTimeToLive() {
        return BytesUtils.extractUnsignedInt(_data.length - 4, _data);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(128);

        builder.append(String.format("Primary NS : %s \n", primaryNameServer()));
        builder.append(String.format("Admin MB : %s \n", adminMailBox()));
        builder.append(String.format("Serial number : %s ", serialNumber()));
        builder.append(String.format("Refresh interval : %s \n", refreshInterval()));
        builder.append(String.format("Retry interval : %s \n", retryInterval()));
        builder.append(String.format("Expiration limit : %s \n", expirationLimit()));
        builder.append(String.format("Minimum TTL : %s \n", minimumTimeToLive()));

        return builder.toString();
    }
}
