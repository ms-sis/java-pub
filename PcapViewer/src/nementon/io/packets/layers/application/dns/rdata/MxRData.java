package nementon.io.packets.layers.application.dns.rdata;

import nementon.io.packets.layers.application.dns.DnsRecordAnswer;
import nementon.io.packets.utils.BytesUtils;

public class MxRData extends RData {

    private byte[] _data;

    public MxRData(DnsRecordAnswer owner, byte[] data) {
        super(owner);

        if (data.length < 2) {
            throw new RuntimeException("Invalid data, size should be at least of 2bytes (16bits) > " + data.length);
        }
    }

    public int preference() {
        return BytesUtils.extractUnsignedShort(0, _data);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("Preference : %s ", preference()));
        // TODO: extract mail exchanger ( may be label, pointer or combinaison of all that shit =DDD)

        return builder.toString();
    }
}
