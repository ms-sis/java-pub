package nementon.io.packets.layers.application;

import nementon.io.packets.visitor.IPacketVisitor;

import java.nio.charset.StandardCharsets;
import java.util.Arrays;

public class HttpPacket extends ApplicationPacket {
    public HttpPacket(byte[] data) {
        header(data);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(header().length);

        builder.append(String.format("Length : %d \n", header().length));
        try {
            builder.append(new String(header(), StandardCharsets.UTF_8));
        } catch (Exception e) {
            builder.append(new String(header()));
        }

        return builder.toString();
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
