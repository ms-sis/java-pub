package nementon.io.packets.layers.link;

import nementon.io.packets.PacketFactory;
import nementon.io.packets.layers.internet.ArpPacket;
import nementon.io.packets.layers.internet.InternetPacket;
import nementon.io.packets.layers.internet.Ipv4Packet;
import nementon.io.packets.layers.internet.Ipv6Packet;
import nementon.io.packets.utils.HardwareAddress;
import nementon.io.packets.visitor.IPacketVisitor;

import java.util.Arrays;

public class EthernetPacket extends LinkPacket {
    static class EthernetField {
        static final int TypeLength = 2;
        static final int HwAddressLength = 6;

        static final int DestinationHwPosition = 0;
        static final int SourceHwPosition = DestinationHwPosition + HwAddressLength;
        static final int TypePosition = SourceHwPosition + HwAddressLength;
        static final int HeaderSize = TypePosition + TypeLength;
    }

    public EthernetPacket(byte[] data) {
        if (data.length < EthernetField.HeaderSize) {
            throw new RuntimeException("Invalid ethernet packet, header size should be at least 14bytes (112bits) > "+ data.length);
        }

        header(
                Arrays.copyOfRange(data, 0, EthernetField.HeaderSize)
        );

        byte[] payload = Arrays.copyOfRange(data, EthernetField.HeaderSize, data.length);
        payloadPacket(
                PacketFactory.Create(ethernetType(), payload));

    }

    public HardwareAddress destinationHwAddress() {
        byte[] hwAddr = Arrays.copyOfRange(
                header(),
                EthernetField.DestinationHwPosition,
                EthernetField.DestinationHwPosition + EthernetField.HwAddressLength);

        return new HardwareAddress(hwAddr);
    }

    public HardwareAddress sourceHwAddress() {
        byte[] hwAddr = Arrays.copyOfRange(
                header(),
                EthernetField.SourceHwPosition,
                EthernetField.SourceHwPosition + EthernetField.HwAddressLength);

        return new HardwareAddress(hwAddr);
    }

    public EthernetPacketType ethernetType() {
        String stringifyHexValue = String.format("%02X%02X", header()[EthernetField.TypePosition], header()[EthernetField.TypePosition + 1]);
        int ethernetTypeValue = Integer.decode("0x"+stringifyHexValue);

        return EthernetPacketType.getEnumFromValue(ethernetTypeValue);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(300);

        builder.append(String.format("Internet layer : %s \n", ethernetType()));
        builder.append(String.format("Hardware destination address %s \n", destinationHwAddress()));
        builder.append(String.format("Hardware source address %s \n", sourceHwAddress()));

        return builder.toString();
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
