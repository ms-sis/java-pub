package nementon.io.packets.layers.link;

import nementon.io.packets.Packet;
import nementon.io.packets.visitor.IPacketVisitor;

public class LinkPacket extends Packet{
    public LinkPacket() {
        super();
    }

    public LinkPacket(byte[] data) {
        header(data);
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
