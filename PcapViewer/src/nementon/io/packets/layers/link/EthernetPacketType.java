package nementon.io.packets.layers.link;

public enum EthernetPacketType  {
    NONE(0x000),
    IPV4(0x800),
    ARP(0x0806),
    REVERSE_ARP(0x8035),
    WAKE_ON_LAN(0x0842),
    APPLE_TALK(0x809B),
    APPLE_TALK_ARP(0x80F3),
    VLAN_TAGGED_FRAME(0x8100),
    NOVELL_INTERNET_NETWORK_PACKET_EXCHANGE(0x8137),
    NOVELL(0x8138),
    IPV6(0x86DD),
    MAC_CONTROL(0x8808),
    COBRANET(0x8819),
    MULTI_PROTOCOL_LABEL_SWITCHING_UNICAST(0x8847),
    MULTI_PROTOCOL_LABEL_SWITCHING_MULTICAST(0x8848),
    POINT_TO_POINT_PROTOCOL_OVER_ETHERNET_DISCOVERY_STAGE(0x8863),
    POINT_TO_POINT_PROTOCOL_OVER_ETHERNET_SESSION_STAGE(0x8864),
    EXTENSIBLE_AUTHENTICATION_PROTOCOLE_OVER_LAN(0x888E),
    HYPER_SCSI(0x889A),
    ATA_OVER_ETHERNET(0x88A2),
    ETHER_CAST_PROTOCOL(0x88A4),
    PROVIDER_BRIDGING(0x88A8),
    AVB_TRANSPORT_PROTOCOL(0x88B5),
    LLDP(0x88CC),
    SERIAL_REAL_TIME_COMMUNICATION_SYSTEM_Iii(0x88CD),
    CIRCUIT_EMULATION_SERVICES_OVER_ETHERNET(0x88D8),
    HOME_PLUG(0x88E1),
    MAC_SECURITY(0x88E5),
    PRECISION_TIME_PROTOCOL(0x88f7),
    CONNECTIVITY_FAULT_MANANGEMENT_OPERATIONS_ADMINISTRATION_MANAGEMENT(0x8902),
    FIBRE_CHANNEL_OVER_ETHERNET_INITIALIZATION_PROTOCOL(0x8914),
    QINQ(0x9100),
    VERITAS_LOW_LATENCY_TRANSPORT(0xCAFE),
    LOOP(0x0060),
    ECHO(0x0200);

    private int _value;
    EthernetPacketType(int value) {
        _value = value;
    }

    public static EthernetPacketType getEnumFromValue(int value) {
        for(EthernetPacketType type : EthernetPacketType.values()) {
            if (type.value() == value) {
                return type;
            }
        }
        return EthernetPacketType.NONE;
    }

    public int value() {
        return _value;
    }
}
