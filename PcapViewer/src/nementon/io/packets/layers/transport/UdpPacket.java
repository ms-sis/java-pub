package nementon.io.packets.layers.transport;

import nementon.io.packets.PacketFactory;
import nementon.io.packets.layers.application.ApplicationPacket;
import nementon.io.packets.layers.application.DnsPacket;
import nementon.io.packets.layers.internet.ip.OfficialUdpPort;
import nementon.io.packets.utils.BytesUtils;
import nementon.io.packets.visitor.IPacketVisitor;

import java.util.Arrays;

public class UdpPacket extends TransportPacket {
    static class UdpField {
        static final int HeaderSize = 8;
        static final int SourcePortPosition = 0;
        static final int DestinationPortPosition = SourcePortPosition + 2;
        static final int LengthPosition = SourcePortPosition + 2;
        static final int ChecksumPosition = LengthPosition + 2;
    }

    public UdpPacket(byte[] data) {
        if (data.length < UdpField.HeaderSize) {
            throw new RuntimeException("Invalid Udp packet, header size should be at lest 8bytes (64bits) > " + data.length);
        }

        header(
                Arrays.copyOfRange(data, 0, UdpField.HeaderSize)
        );

        byte[] payload = Arrays.copyOfRange(data, UdpField.HeaderSize, data.length);
        payloadPacket(
                PacketFactory.Create(this, payload));
    }

    public int sourcePort() {
        return BytesUtils.extractUnsignedShort(
                UdpField.SourcePortPosition,
                header()
        );
    }

    public int destinationPort() {
        return BytesUtils.extractUnsignedShort(
                UdpField.DestinationPortPosition,
                header()
        );
    }

    public int length() {
        return BytesUtils.extractUnsignedShort(
                UdpField.LengthPosition,
                header()
        );
    }

    public int checksum() {
        return BytesUtils.extractUnsignedShort(
                UdpField.ChecksumPosition,
                header()
        );
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(256);

        builder.append(String.format("Source port : %d \n", sourcePort()));
        builder.append(String.format("Destination port : %d \n", destinationPort()));
        builder.append(String.format("Length : %d \n", length()));
        builder.append(String.format("Checksum : %d \n", checksum()));

        return builder.toString();
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
