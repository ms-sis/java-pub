package nementon.io.packets.layers.transport;

import nementon.io.packets.PacketFactory;
import nementon.io.packets.layers.application.ApplicationPacket;
import nementon.io.packets.layers.application.DnsPacket;
import nementon.io.packets.layers.application.FtpPacket;
import nementon.io.packets.layers.application.HttpPacket;
import nementon.io.packets.layers.internet.ip.OfficialTcpPort;
import nementon.io.packets.utils.BytesUtils;
import nementon.io.packets.visitor.IPacketVisitor;

import java.lang.reflect.Method;
import java.util.Arrays;

public class TcpPacket extends TransportPacket {
    static class TcpField {
        static final int HeaderSize = 20;

        static final int SourcePortPosition = 0;
        static final int DestinationPortPosition = SourcePortPosition + 2;
        static final int SequenceNumberPosition = DestinationPortPosition + 2;
        static final int ACKPosition = SequenceNumberPosition + 4;
        static final int DataOffsetPosition = ACKPosition + 4;
        static final int ReversedPosition = DataOffsetPosition;
        static final int FlagsPosition = ReversedPosition;
        static final int WindowSizePosition = FlagsPosition + 2;
        static final int ChecksumPosition = WindowSizePosition + 2;
        static final int UrgentPointerPosition = ChecksumPosition + 2;
        static final int OptionPosition = UrgentPointerPosition + 2;
    }

    static class TcpFlagsMask {
        static final int NS = 0x01;
        static final int CWR = 0x80;
        static final int ECE = 0x40;
        static final int URG = 0x20;
        static final int ACK = 0x10;
        static final int PSH = 0x08;
        static final int RST = 0x04;
        static final int SYN = 0x02;
        static final int FIN = 0x01;
    }

    private byte[] _optionData;

    public TcpPacket(byte[] data) {
        if (data.length < TcpField.HeaderSize) {
            throw new RuntimeException("Invalid TCP packet, header size should be at least 20bytes (160bits) > " + data.length);
        }

        header(
                Arrays.copyOfRange(
                        data,
                        0,
                        TcpField.HeaderSize
                )
        );

        _optionData = new byte[0];
        int dataOffset = dataOffset();
        if (dataOffset > 5) {
            if (dataOffset > 15) {
                throw new RuntimeException("Invalid TCP Packet, dataOffset ("+ dataOffset +") should be between 5 && 15"); }

            int endOfHeaderWithOptions = TcpField.HeaderSize + ( 5 - dataOffset );
            if (endOfHeaderWithOptions > data.length) {
                throw new RuntimeException("Invalid TCP Packet, dataOffset should not be superior to packet size"); }

            _optionData = Arrays.copyOfRange(
                    data,
                    TcpField.HeaderSize,
                    endOfHeaderWithOptions);
        }

        byte[] payload = Arrays.copyOfRange(
                data,
                TcpField.HeaderSize + _optionData.length,
                data.length
        );
        payloadPacket(
                PacketFactory.Create(this, payload));
    }

    @Override
    public int totalLength() {
        return super.totalLength() + _optionData.length;
    }

    public int sourcePort() {
        return BytesUtils.extractUnsignedShort(
                TcpField.SourcePortPosition,
                header()
        );
    }

    public int destinationPort() {
        return BytesUtils.extractUnsignedShort(
                TcpField.DestinationPortPosition,
                header()
        );
    }

    public long sequenceNumber() {
        return BytesUtils.extractUnsignedInt(
                TcpField.SequenceNumberPosition,
                header()
        );
    }

    public long acknowledgment() {
       return BytesUtils.extractUnsignedInt(
               TcpField.ACKPosition,
               header()
       );
    }

    public int dataOffset() {
        return header()[TcpField.DataOffsetPosition] & 0xF >> 4;
    }

    public int reserved() {
        return 0;
    }

    public boolean NS() {
        return (flags()[0] & TcpFlagsMask.NS) != 0;
    }

    public boolean CWR() {
        return (flags()[1] & TcpFlagsMask.CWR) != 0;
    }

    public boolean ECE() {
        return (flags()[1] & TcpFlagsMask.ECE) != 0;
    }

    public boolean URG() {
        return (flags()[1] & TcpFlagsMask.URG) != 0;
    }

    public boolean ACK() {
        return (flags()[1] & TcpFlagsMask.ACK) != 0;
    }

    public boolean PSH() {
        return (flags()[1] & TcpFlagsMask.PSH) != 0;
    }

    public boolean RST() {
        return (flags()[1] & TcpFlagsMask.RST) != 0;
    }

    public boolean SYN() {
        return (flags()[1] & TcpFlagsMask.SYN) != 0;
    }

    public boolean FIN() {
        return (flags()[1] & TcpFlagsMask.FIN) != 0;
    }

    public byte[] flags() {
        byte[] flags = new byte[2];
        if ( (header()[TcpField.FlagsPosition] & (1)) != 0) {
            flags[0] = (byte) 0x01;
        }
        flags[1] = header()[TcpField.FlagsPosition + 1];
        return flags;
    }

    public int windowSize() {
        return BytesUtils.extractUnsignedShort(
                TcpField.WindowSizePosition,
                header()
        );
    }

    public int checksum() {
        return BytesUtils.extractUnsignedShort(
                TcpField.ChecksumPosition,
                header()
        );
    }

    public int urgentPointer() {
        return BytesUtils.extractUnsignedShort(
                TcpField.UrgentPointerPosition,
                header()
        );
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1200);

        builder.append(String.format("Source port : %d \n", sourcePort()));
        builder.append(String.format("Destination port : %d \n", destinationPort()));
        builder.append(String.format("Sequence number : %d \n", sequenceNumber()));
        builder.append(String.format("ACK : %d \n", acknowledgment()));
        builder.append(String.format("Data offset : %d \n", dataOffset()));
        builder.append(String.format("Flags : ...%d %d%d%d%d %d%d%d%d \n",
                hasFlags("NS"),
                hasFlags("CWR"),
                hasFlags("ECE"),
                hasFlags("URG"),
                hasFlags("ACK"),
                hasFlags("PSH"),
                hasFlags("RST"),
                hasFlags("SYN"),
                hasFlags("FIN")));
        builder.append(String.format("Window Size : %d \n", windowSize()));
        builder.append(String.format("Checksum : %d \n", checksum()));
        builder.append(String.format("Urgent pointer : %d \n", urgentPointer()));

        return builder.toString();
    }

    private int hasFlags(String flagName) {
        int isFlagSet = 0;

        try {
            Method hasFlagMethod = getClass().getMethod(flagName);
            if ((boolean)hasFlagMethod.invoke(this)) {
                isFlagSet = 1;
            }
        } catch (Exception ignored) {}
        return isFlagSet;
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
