package nementon.io.packets.layers.transport;

import nementon.io.packets.Packet;
import nementon.io.packets.visitor.IPacketVisitor;

public class TransportPacket extends Packet {
    public TransportPacket() {
        super();
    }

    public TransportPacket(byte[] payload) {
        header(payload);
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
