package nementon.io.packets.layers.internet.icmp;

import nementon.io.packets.layers.internet.IcmpPacket;

import java.net.InetAddress;
import java.util.Arrays;

public class RedirectMessage extends IcmpControlMessage {
    private byte[] _message;
    private IcmpPacket _owner;

    public RedirectMessage(byte[] message, IcmpPacket owner) {
        if (message.length < 8) {
            throw new RuntimeException("Invalid Icmp Redirect message, it's size should be at least of 8bytes");
        }
        _message = message;
        _owner = owner;
    }

    public InetAddress ipAddress() {
        try {
            return InetAddress.getByAddress(
                    Arrays.copyOfRange(_message, 0, 4)
            );
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("Ip address : %s ", ipAddress()));
        switch (_owner.code()) {
            case 0:
                builder.append(" (Redirect for Network) \n");
                break;
            case 1:
                builder.append(" (Redirect for Host) \n");
                break;
            case 2:
                builder.append(" (Redirect for Type of Service and Network) \n");
                break;
            case 3:
                builder.append(" (Redirect for Type of Service and Host) \n");
                break;
            default:
                builder.append(" (Unknown message) \n");
        }

        return builder.toString();
    }
}
