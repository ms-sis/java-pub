package nementon.io.packets.layers.internet;

import nementon.io.packets.Packet;
import nementon.io.packets.visitor.IPacketVisitor;

public class InternetPacket extends Packet {
    public InternetPacket() {
        super();
    }

    public InternetPacket(byte[] data) {
        header(data);
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
