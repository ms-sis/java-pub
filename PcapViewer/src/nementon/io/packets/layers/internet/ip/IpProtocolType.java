package nementon.io.packets.layers.internet.ip;


public enum IpProtocolType {
    IP(0),
    ICMP(1),
    IGMP(2),
    IPIP(4),
    TCP(6),
    EGP(8),
    PUP(12),
    UDP(17),
    IDP(22),
    TP(29),
    IPV6(41),
    ROUTING(43),
    FRAGMENT(44),
    RSVP(46),
    GRE(47),
    ESP(50),
    AH(51),
    ICMPV6(58),
    NONE(59),
    DSTOPTS(60),
    MTP(92),
    ENCAP(98),
    PIM(103),
    COMP(108),
    RAW(255),
    MASK(0xff);

    private int _value;
    IpProtocolType(int value) {
        _value = value;
    }

    public static IpProtocolType getEnumFromValue(int value) {
        for(IpProtocolType type : IpProtocolType.values()) {
            if (type.value() == value) {
                return type;
            }
        }
        return IpProtocolType.NONE;
    }

    public int value() {
        return _value;
    }
}
