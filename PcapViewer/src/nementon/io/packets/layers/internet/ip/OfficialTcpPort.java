package nementon.io.packets.layers.internet.ip;

public enum OfficialTcpPort {
    UNKNOWN(-1),
    FTP(21),
    FTP_DATA(20),
    FTPS(990),
    SSH(22),
    HTTP(80),
    HTTPS(443),
    TCPMUX(1),
    CNMU(2),
    CNCP(3),
    RJE(5),
    ECHO(7),
    DSICARD(9),
    ACTIVE_USERS(11),
    DAYTIME(13),
    NETSTAT(15),
    QUOTE_OF_THE_DAY(17),
    TELNET(23),
    SMTP(25),
    NSW(27),
    MSG_IP(29),
    TIME(37),
    WHOIS(43),
    DNS(53);

    private int _value;
    OfficialTcpPort(int value) {
        _value = value;
    }

    public static OfficialTcpPort getEnumFormValue(int value) {
        for(OfficialTcpPort tcpPort : OfficialTcpPort.values()) {
            if (tcpPort.value() == value) {
                return tcpPort;
            }
        }
        return OfficialTcpPort.UNKNOWN;
    }

    public int value() {
        return _value;
    }
}
