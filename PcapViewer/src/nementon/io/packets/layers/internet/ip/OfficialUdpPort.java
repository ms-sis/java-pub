package nementon.io.packets.layers.internet.ip;

public enum OfficialUdpPort {
    UNKNOWN(-1),
    FTP(21),
    FTP_DATA(20),
    FTPS(90),
    SSH(22),
    HTTP(80),
    HTTPS(443),
    TCPMUX(1),
    TELNET(23),
    SMTP(25),
    WHOIS(43),
    DNS(53);


    private int _value;
    OfficialUdpPort(int value) {
        _value = value;
    }

    public static OfficialUdpPort getEnumFromValue(int value) {
        for (OfficialUdpPort udpPort : OfficialUdpPort.values()) {
            if (udpPort.value() == value) {
                return udpPort;
            }
        }
        return OfficialUdpPort.UNKNOWN;
    }

    public int value() {
        return _value;
    }
}
