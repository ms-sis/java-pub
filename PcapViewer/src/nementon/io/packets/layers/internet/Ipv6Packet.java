package nementon.io.packets.layers.internet;

import nementon.io.packets.visitor.IPacketVisitor;

import java.util.Vector;

public class Ipv6Packet extends InternetPacket {
    public Ipv6Packet(byte[] data) {
        //throw new RuntimeException("Not implemented yet");
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
