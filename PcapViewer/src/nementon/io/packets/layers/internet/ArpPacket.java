package nementon.io.packets.layers.internet;

import nementon.io.packets.layers.internet.arp.ArpMessageType;
import nementon.io.packets.layers.link.EthernetPacketType;
import nementon.io.packets.utils.BytesUtils;
import nementon.io.packets.utils.HardwareAddress;
import nementon.io.packets.visitor.IPacketVisitor;
import nementon.io.pcap.PcapNetworkLinkLayer;

import java.net.InetAddress;
import java.util.Arrays;

public class ArpPacket extends InternetPacket {
    static class ArpField {
        static final int HeaderSize = 26;
        static final int HardwareTypePosition = 0;
        static final int ProtocolTypePosition = HardwareTypePosition + 2;
        static final int HardwareAddressLengthPosition = ProtocolTypePosition + 2;
        static final int ProtocolAddressLengthPosition = HardwareAddressLengthPosition + 1;
        static final int OperationPosition =ProtocolAddressLengthPosition + 1;
        static final int SenderHardwareAddressPosition = OperationPosition +2;
        static final int SenderProtocolAddressPosition = SenderHardwareAddressPosition + 6;
        static final int TargetHardwareAddressPosition = SenderProtocolAddressPosition + 4;
        static final int TargetProtocolAddressPosition = TargetHardwareAddressPosition + 6;
    }

    public ArpPacket(byte[] data) {

        header(data);

    }

    public PcapNetworkLinkLayer hardwareType() {
        return PcapNetworkLinkLayer.getEnumFromValue(
                BytesUtils.extractUnsignedShort(
                        ArpField.HardwareTypePosition,
                        header()
                )
        );
    }

    public EthernetPacketType protocolType() {
        return EthernetPacketType.getEnumFromValue(
                BytesUtils.extractUnsignedShort(
                        ArpField.ProtocolTypePosition,
                        header()
                )
        );
    }

    public int hardwareAddressLength() {
        return header()[ArpField.HardwareAddressLengthPosition];
    }

    public int protocolAddressLength() {
        return header()[ArpField.ProtocolAddressLengthPosition];
    }

    public ArpMessageType operation() {
        return ArpMessageType.getEnumFromValue(
            BytesUtils.extractUnsignedShort(
                    ArpField.OperationPosition,
                    header()
            )
        );
    }

    HardwareAddress senderHardwareAddress() {
        return new HardwareAddress(
                Arrays.copyOfRange(
                        header(),
                        ArpField.SenderHardwareAddressPosition,
                        ArpField.SenderProtocolAddressPosition
                        )
        );
    }

    public InetAddress senderProtocolAddress() {
        try {
            return InetAddress.getByAddress(
                    Arrays.copyOfRange(
                            header(),
                            ArpField.SenderProtocolAddressPosition,
                            ArpField.TargetHardwareAddressPosition
                    )
            );
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    HardwareAddress targetHardwareAddress() {
        return new HardwareAddress(
                Arrays.copyOfRange(
                        header(),
                        ArpField.TargetHardwareAddressPosition,
                        ArpField.TargetProtocolAddressPosition
                )
        );
    }

    public InetAddress targetProtocolAddress() {
        try {
            return InetAddress.getByAddress(
                    Arrays.copyOfRange(
                            header(),
                            ArpField.TargetProtocolAddressPosition,
                            ArpField.TargetProtocolAddressPosition + 4
                    )
            );
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();

        builder.append(String.format("Hardware type : %s \n", hardwareType()));
        builder.append(String.format("Protocol type %s \n", protocolType()));
        builder.append(String.format("Hardware address length : %d \n", hardwareAddressLength()));
        builder.append(String.format("Protocol address length %d \n", protocolAddressLength()));
        builder.append(String.format("Operation : %s (%d) \n", operation(), operation().value()));
        builder.append(String.format("Sender hardware address : %s \n", senderHardwareAddress()));
        builder.append(String.format("Sender protocol address : %s \n", senderProtocolAddress()));
        builder.append(String.format("Target hardware address : %s \n", targetHardwareAddress()));
        builder.append(String.format("Target protocol address : %s \n", targetProtocolAddress()));


        return builder.toString();
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
