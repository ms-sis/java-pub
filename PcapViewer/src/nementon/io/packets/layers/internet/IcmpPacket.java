package nementon.io.packets.layers.internet;

import nementon.io.packets.layers.internet.icmp.IcmpControlMessage;
import nementon.io.packets.layers.internet.icmp.RedirectMessage;
import nementon.io.packets.utils.BytesUtils;
import nementon.io.packets.visitor.IPacketVisitor;

import java.util.Arrays;

public class IcmpPacket extends InternetPacket {
    static class IcmpField {
        static final int HeaderSize = 4;
        static final int TypePosition = 0;
        static final int CodePosition = TypePosition + 1;
        static final int ChecksumPosition = CodePosition + 1;
        static final int DataPosition = ChecksumPosition + 2;
    }

    private IcmpControlMessage _controlMessage;
    private byte[] _rawData;

    public IcmpPacket(byte[] data) {
        if (data.length < IcmpField.HeaderSize) {
            throw new RuntimeException("Invalid Icmp packet, header size should be at lest 8bytes (64bits) > " + data.length);
        }

        header(
                Arrays.copyOfRange(data, 0, IcmpField.HeaderSize)
        );

        _rawData = Arrays.copyOfRange(data, IcmpField.HeaderSize, data.length);
        // TODO: clean up
        switch (type()) {
            case 3:
                _controlMessage = new IcmpControlMessage() {
                    @Override
                    public String toString() {
                        switch (code()) {
                            case 0:
                                return "Destination network unreachable";
                            case 1:
                                return "Destination host unreachable";
                            case 2:
                                return "Destination protocol unreachable";
                            case 3:
                                return "Destination port unreachable";
                            case 4:
                                return "Fragmentation required";
                            case 5:
                                return "Source route failed";
                            case 6:
                                return "Destination network unknown";
                            case 7:
                                return "Destination host unknown";
                            case 8:
                                return "Source host isolated";
                            case 9:
                                return "Network administratively prohibited";
                            case 10:
                                return "Host administratively prohibited";
                            case 11:
                                return "Network unreachable for TOS";
                            case 12:
                                return "Host unreachable for TOS";
                            case 13:
                                return "Communication administratively prohibited";
                            case 14:
                                return "Host precedence violation";
                            case 15:
                                return "Precedence cutoff in effect";
                        }
                        return "Destination unreachable";
                    }
                };
                break;
            case 5:
                _controlMessage = new RedirectMessage(_rawData, this);
                break;
            default:
                _controlMessage = new IcmpControlMessage() {
                    @Override
                    public String toString() {
                        switch (type()) {
                            case 0:
                                return "Echo Reply";
                            case 8:
                                return "Echo Request";
                            case 1:
                            case 2:
                            case 4:
                            case 6:
                            case 15:
                            case 16:
                            case 17:
                            case 18:
                            case 30:
                            case 31:
                            case 32:
                            case 33:
                            case 34:
                            case 35:
                            case 36:
                            case 37:
                            case 38:
                            case 39:
                                return "Deprecated, ignored !";
                            case 9:
                                return "Router advertisement";
                            case 10:
                                return "Router solicitation";
                            case 11:
                                return "Time exceeded";
                            case 12:
                                return "Parameter problem: Bad IP header";
                            case 13:
                                return "Timestamp";
                            case 14:
                                return "Timestamp reply";
                            case 40:
                                return "Security failure";
                            case 42:
                                return "The answer to life, the universe and everything is 42";
                        }
                        return "Unknown message \n";
                    }
                };
        }
    }

    public int type() {
        return header()[IcmpField.TypePosition] & 0xFF;
    }

    public int code() {
        return header()[IcmpField.CodePosition] & 0xFF;
    }

    public int checksum() {
        return BytesUtils.extractUnsignedShort(
                IcmpField.ChecksumPosition,
                header()
        );
    }

    public byte[] data() {
        return _rawData;
    }

    public IcmpControlMessage message() {
        return _controlMessage;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(256);

        builder.append(String.format("Type : %d \n", type()));
        builder.append(String.format("Code : %d \n", code()));
        builder.append(String.format("Checksum : %d \n", checksum()));
        builder.append(String.format("Control message : %s", message()));

        return builder.toString();
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
