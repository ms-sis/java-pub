package nementon.io.packets.layers.internet;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.nio.ByteBuffer;
import java.util.Arrays;

import nementon.io.packets.PacketFactory;
import nementon.io.packets.layers.internet.ip.IpProtocolType;
import nementon.io.packets.utils.BytesUtils;
import nementon.io.packets.visitor.IPacketVisitor;

public class Ipv4Packet extends InternetPacket {
    static class Ipv4Field {
        static final int IpAddressLength = 4;
        static final int HeaderSize = 20;

        static final int VersionPosition = 0;
        static final int IHLPosition = 0;
        static final int DSCPPosition = IHLPosition +  1;
        static final int ECNPosition = IHLPosition + 1;
        static final int TotalLengthPosition = 2;
        static final int IdentificationPosition = 4;
        static final int FlagsPosition = 6;
        static final int FragmentOffsetPosition = 6;
        static final int TimeToLivePosition = 8;
        static final int ProtocolPosition = 9;
        static final int HeaderChecksumPosition = 10;
        static final int SourceIpAddressPosition = 12;
        static final int DestinationIpAddressPosition = 16;
    }

    private byte[] _optionData;

    public Ipv4Packet(byte[] data) {
        if (data.length < Ipv4Field.HeaderSize) {
            throw new RuntimeException("Invalid Ipv4 packet, header size should be at least 20bytes (160bits) > " + data.length);
        }

        header(
            Arrays.copyOfRange(data, 0, Ipv4Field.HeaderSize)
        );


        int internetHeaderLength = internetHeaderLength();
        if (internetHeaderLength < 5 || internetHeaderLength > 15) {
            throw new RuntimeException("IHL should be between 5 && 15");
        }

        if (internetHeaderLength > 5) {
            if (totalLength() > data.length) {
                throw new RuntimeException("Invalid Ipv4 packet, packet size is too low");
            }

            _optionData = Arrays.copyOfRange(
                    data,
                    Ipv4Field.HeaderSize,
                    totalLength());
        }
        else {
            _optionData = new byte[0];
        }

        byte[] payload = Arrays.copyOfRange(
                data,
                header().length + _optionData.length,
                data.length);

        payloadPacket(
                PacketFactory.Create(protocolType(), payload)
        );
    }

    public int version() {
        return header()[Ipv4Field.VersionPosition] >> 4;
    }

    public int internetHeaderLength() {
        return header()[Ipv4Field.IHLPosition] & 0x0F;
    }

    public int differentiatedServicesCodePoint() {
        return header()[Ipv4Field.DSCPPosition] >> 2;
    }

    public int explicitCongestionNotification() {
        return header()[Ipv4Field.ECNPosition] & 0x03;
    }

    @Override
    public int totalLength() {
        return BytesUtils.extractUnsignedShort(
                Ipv4Field.TotalLengthPosition,
                header());
    }

    public int identification(){
        return BytesUtils.extractUnsignedShort(
                Ipv4Field.IdentificationPosition,
                header()
        );
    }

    public int flags() {
        return header()[Ipv4Field.FlagsPosition] >> 5;
    }

    public long fragmentOffset() {
        ByteBuffer buffer = ByteBuffer.allocate(13);
        buffer.put(
                Arrays.copyOfRange(
                        header(),
                        Ipv4Field.FragmentOffsetPosition,
                        Ipv4Field.FragmentOffsetPosition + 13
                )
        );

        byte b;

        return buffer.get(0);
    }

    public int timeToLive() {
        return header()[Ipv4Field.TimeToLivePosition] & 0xFF;
    }

    public IpProtocolType protocolType() {
        return IpProtocolType.getEnumFromValue(
                header()[Ipv4Field.ProtocolPosition] & 0xFF
        );
    }

    public int headerChecksum() {
        return BytesUtils.extractUnsignedShort(
                Ipv4Field.HeaderChecksumPosition,
                header()
        );
    }

    public InetAddress sourceIdAddress() {
        byte[] ipAddr = Arrays.copyOfRange(
                header(),
                Ipv4Field.SourceIpAddressPosition,
                Ipv4Field.SourceIpAddressPosition + Ipv4Field.IpAddressLength);

        try {
            return Inet4Address.getByAddress(ipAddr);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public InetAddress destinationIpAddress() {
        byte[] ipAddr = Arrays.copyOfRange(
                header(),
                Ipv4Field.DestinationIpAddressPosition,
                Ipv4Field.DestinationIpAddressPosition + Ipv4Field.IpAddressLength);

        try {
            return Inet4Address.getByAddress(ipAddr);
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder(1200);

        builder.append(String.format("Version : %d \n", version()));
        builder.append(String.format("IHL : %d \n", internetHeaderLength()));
        builder.append(String.format("DSCP : %d \n", differentiatedServicesCodePoint()));
        builder.append(String.format("ECN : %d \n", explicitCongestionNotification()));
        builder.append(String.format("Total length : %d \n", totalLength()));
        builder.append(String.format("Identification : 0x%04x (%d) \n", identification(), identification()));
        builder.append(String.format("Flags : %d \n", flags()));
        builder.append(String.format("Fragment offset : %d \n", fragmentOffset()));
        builder.append(String.format("TTL : %d \n", timeToLive()));
        builder.append(String.format("Protocol : %s (%d) \n", protocolType(), protocolType().value()));
        builder.append(String.format("Header checksum %d \n", headerChecksum()));
        builder.append(String.format("Source ip Address : %s \n", sourceIdAddress()));
        builder.append(String.format("Destination ip address : %s \n", destinationIpAddress()));

        return builder.toString();
    }

    public void accept(IPacketVisitor visitor) {
        visitor.visit(this);

        if (payloadPacket() != null) {
            payloadPacket().accept(visitor);
        }
    }
}
