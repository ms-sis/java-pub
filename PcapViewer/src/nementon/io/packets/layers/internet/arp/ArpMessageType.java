package nementon.io.packets.layers.internet.arp;

public enum  ArpMessageType {
    UNKNOWN(-1),
    ARP_REQUEST(1),
    ARP_REPLY(2),
    RARP_REQUEST(3),
    RARP_REPLY(4),
    DRARP_REQUEST(5),
    DRARP_REPLY(6),
    DRARP_ERROR(7),
    InARP_REQUEST(8),
    InARP_REPLY(9);


    private int _value;
    ArpMessageType(int value) {
        _value = value;
    }

    public static ArpMessageType getEnumFromValue(int value) {
        for(ArpMessageType msg : ArpMessageType.values()) {
            if (msg.value() == value) {
                return msg;
            }
        }
        return ArpMessageType.UNKNOWN;
    }

    public int value() {
        return _value;
    }
}
