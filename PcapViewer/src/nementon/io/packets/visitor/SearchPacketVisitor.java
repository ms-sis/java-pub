package nementon.io.packets.visitor;

import nementon.io.packets.Packet;
import nementon.io.packets.layers.application.ApplicationPacket;
import nementon.io.packets.layers.application.DnsPacket;
import nementon.io.packets.layers.application.FtpPacket;
import nementon.io.packets.layers.application.HttpPacket;
import nementon.io.packets.layers.internet.*;
import nementon.io.packets.layers.link.EthernetPacket;
import nementon.io.packets.layers.link.LinkPacket;
import nementon.io.packets.layers.transport.TcpPacket;
import nementon.io.packets.layers.transport.TransportPacket;
import nementon.io.packets.layers.transport.UdpPacket;

import java.util.LinkedList;
import java.util.List;

public class SearchPacketVisitor implements IPacketVisitor {

    private List<Packet> _packets;
    private PacketFilters _filters;

    public SearchPacketVisitor(PacketFilters filters) {
        _packets = new LinkedList<>();
        _filters = filters;
    }

    @Override
    public void visit(LinkPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(EthernetPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(InternetPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(ArpPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(IcmpPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(Ipv4Packet packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(Ipv6Packet packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(TransportPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(TcpPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(UdpPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(ApplicationPacket packet) {
        if (_filters.match(packet)) {
            addPacket(packet);
        }
    }

    @Override
    public void visit(DnsPacket dnsPacket) {
        if (_filters.match(dnsPacket)) {
            addPacket(dnsPacket);
        }
    }

    @Override
    public void visit(FtpPacket ftpPacket) {
        if (_filters.match(ftpPacket)) {
            addPacket(ftpPacket);
        }
    }

    @Override
    public void visit(HttpPacket httpPacket) {
        if (_filters.match(httpPacket)) {
            addPacket(httpPacket);
        }
    }

    private void addPacket(Packet packet) {
        while (packet.parentPacket() != null) {
            packet = packet.parentPacket();
        }

        if (!_packets.contains(packet)) {
            _packets.add(packet);
        }
    }

    public List<Packet> matchedPackets() {
        return _packets;
    }
}
