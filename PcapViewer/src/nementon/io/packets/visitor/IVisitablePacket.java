package nementon.io.packets.visitor;
public interface IVisitablePacket {
    void accept(IPacketVisitor visitor);
}
