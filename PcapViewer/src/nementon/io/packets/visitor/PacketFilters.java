package nementon.io.packets.visitor;

import nementon.io.packets.layers.application.ApplicationPacket;
import nementon.io.packets.layers.application.DnsPacket;
import nementon.io.packets.layers.application.FtpPacket;
import nementon.io.packets.layers.application.HttpPacket;
import nementon.io.packets.layers.internet.*;
import nementon.io.packets.layers.link.EthernetPacket;
import nementon.io.packets.layers.link.LinkPacket;
import nementon.io.packets.layers.transport.TcpPacket;
import nementon.io.packets.layers.transport.TransportPacket;
import nementon.io.packets.layers.transport.UdpPacket;

public class PacketFilters {
    private String _linkLayer;
    private String _internetLayer;
    private String _transportLayer;
    private String _applicationLayer;

    public PacketFilters(String linkLayer, String internetLayer, String transportLayer, String applicationLayer) {
        if (linkLayer == null && internetLayer == null && transportLayer == null && applicationLayer == null) {
            _linkLayer = _internetLayer = _transportLayer = _applicationLayer = "*";
        }
        else {
            _linkLayer = linkLayer != null ? linkLayer : "";
            _internetLayer = internetLayer != null ? internetLayer : "";
            _transportLayer = transportLayer != null ? transportLayer : "";
            _applicationLayer = applicationLayer != null ? applicationLayer : "";
        }

    }

    public boolean match(LinkPacket packet) {
        if (_linkLayer.equals("*")) {
            return true;
        }
        else if (_linkLayer.equalsIgnoreCase("ethernet") && packet instanceof EthernetPacket) {
            return true;
        }

        return false;
    }

    public boolean match(InternetPacket packet) {
        if (_internetLayer.equals("*")) {
            return true;
        }
        else if (_internetLayer.equalsIgnoreCase("arp") && packet instanceof ArpPacket) {
            return true;
        }
        else if (_internetLayer.equalsIgnoreCase("ip") && ( packet instanceof Ipv4Packet || packet instanceof Ipv6Packet)) {
            return true;
        }
        else if (_internetLayer.equalsIgnoreCase("icmp") && packet instanceof IcmpPacket) {
            return true;
        }

        return false;
    }

    public boolean match(TransportPacket packet) {
        if (_transportLayer.equals("*")) {
            return true;
        }
        else if (_transportLayer.equalsIgnoreCase("upd") && packet instanceof UdpPacket) {
            return true;
        }
        else if( _transportLayer.equalsIgnoreCase("tcp") && packet instanceof TcpPacket) {
            return true;
        }

        return false;
    }

    public boolean match(ApplicationPacket packet) {
        if (_applicationLayer.equals("*")) {
            return true;
        }
        else if (_applicationLayer.equalsIgnoreCase("http") && packet instanceof HttpPacket) {
            return true;
        }
        else if (_applicationLayer.equalsIgnoreCase("ftp") && packet instanceof FtpPacket) {
            return true;
        }
        else if (_applicationLayer.equalsIgnoreCase("dns") && packet instanceof DnsPacket) {
            return true;
        }

        return false;
    }
}
