package nementon.io.packets.visitor;

import nementon.io.packets.layers.application.ApplicationPacket;
import nementon.io.packets.layers.application.DnsPacket;
import nementon.io.packets.layers.application.FtpPacket;
import nementon.io.packets.layers.application.HttpPacket;
import nementon.io.packets.layers.internet.*;
import nementon.io.packets.layers.link.EthernetPacket;
import nementon.io.packets.layers.link.LinkPacket;
import nementon.io.packets.layers.transport.TcpPacket;
import nementon.io.packets.layers.transport.TransportPacket;
import nementon.io.packets.layers.transport.UdpPacket;

public interface IPacketVisitor {
    // Application layer
    void visit(ApplicationPacket packet);
    void visit(DnsPacket dnsPacket);
    void visit(FtpPacket ftpPacket);
    void visit(HttpPacket httpPacket);

    // Internet layer
    void visit(InternetPacket packet);
    void visit(ArpPacket packet);
    void visit(IcmpPacket packet);
    void visit(Ipv4Packet packet);
    void visit(Ipv6Packet packet);

    // Transport layer
    void visit(TransportPacket packet);
    void visit(TcpPacket packet);
    void visit(UdpPacket packet);

    // Link layer
    void visit(LinkPacket packet);
    void visit(EthernetPacket packet);
}
