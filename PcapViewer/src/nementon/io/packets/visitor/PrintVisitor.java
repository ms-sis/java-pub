package nementon.io.packets.visitor;

import nementon.io.packets.layers.application.ApplicationPacket;
import nementon.io.packets.layers.application.DnsPacket;
import nementon.io.packets.layers.application.FtpPacket;
import nementon.io.packets.layers.application.HttpPacket;
import nementon.io.packets.layers.internet.*;
import nementon.io.packets.layers.link.EthernetPacket;
import nementon.io.packets.layers.link.LinkPacket;
import nementon.io.packets.layers.transport.TcpPacket;
import nementon.io.packets.layers.transport.TransportPacket;
import nementon.io.packets.layers.transport.UdpPacket;

public class PrintVisitor implements IPacketVisitor {
    @Override
    public void visit(ApplicationPacket packet) {
        System.out.println("UNKNOWN APPLICATION LAYER -------------------------------------------");
    }

    @Override
    public void visit(DnsPacket dnsPacket) {
        System.out.println(String.format("DNS -----------------------------------------------------------------"));
        System.out.println("|\t" + dnsPacket.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(FtpPacket ftpPacket) {
        System.out.println(String.format("FTP -----------------------------------------------------------------"));
        System.out.println("|\t" + ftpPacket.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(HttpPacket httpPacket) {
        System.out.println(String.format("HTTP ----------------------------------------------------------------"));
        System.out.println("|\t" + httpPacket.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(InternetPacket packet) {
        System.out.println("UNKNOWN INTERNET LAYER ----------------------------------------------");
    }

    @Override
    public void visit(ArpPacket packet) {
        System.out.println(String.format("ARP -----------------------------------------------------------------"));
        System.out.println("|\t" + packet.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(IcmpPacket packet) {
        System.out.println(String.format("ICMP ----------------------------------------------------------------"));
        System.out.println("|\t" + packet.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(Ipv4Packet packet) {
        System.out.println(String.format("IPV4 ----------------------------------------------------------------"));
        System.out.println("|\t" + packet.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(Ipv6Packet packet) {
        System.out.println(String.format("IPV6 ----------------------------------------------------------------"));
        System.out.println("|\t" + packet.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(TransportPacket packet) {
        System.out.println("UNKNOWN TRANSPORT LAYER ---------------------------------------------");
    }

    @Override
    public void visit(TcpPacket packet) {
        System.out.println(String.format("TCP -----------------------------------------------------------------"));
        System.out.println("|\t" + packet.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(UdpPacket packet) {
        System.out.println(String.format("UDP -----------------------------------------------------------------"));
        System.out.println("|\t" + packet.toString().replaceAll("\\n", "\n|\t"));
    }

    @Override
    public void visit(LinkPacket packet) {
        System.out.println("UNKNOWN LINK LAYER ------------------------------------------------------------");
    }

    @Override
    public void visit(EthernetPacket packet) {

        System.out.println(String.format("\nETHERNET ------------------------------------------------------------"));
        System.out.println("|\t" + packet.toString().replaceAll("\\n", "\n|\t"));
    }
}
