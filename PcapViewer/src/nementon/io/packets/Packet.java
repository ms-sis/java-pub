package nementon.io.packets;


import nementon.io.packets.visitor.IPacketVisitor;
import nementon.io.packets.visitor.IVisitablePacket;

public abstract class Packet implements IVisitablePacket {

    private byte[] _header;
    private Packet _parentPacket;
    private Packet _payloadPacket;

    public Packet()
    {
        _header = new byte[0];
    }

    public int totalLength() {
        int length = _header.length;
        if (_payloadPacket != null) {
            length += _payloadPacket.totalLength();
        }
        return length;
    }

    public Packet parentPacket() {
        return _parentPacket;
    }

    public void parentPacket(Packet parent) {
        _parentPacket = parent;
    }

    public byte[] header() {
        return _header;
    }

    public void header(byte[] header) {
        if (header == null) { throw new IllegalArgumentException("Header data could not be null"); }
        _header = header;
    }

    public Packet payloadPacket(){
        return _payloadPacket;
    }

    public void payloadPacket(Packet payload) {
        if (payload == null) { throw new IllegalArgumentException("Packet payload data could not be null"); }
        if (payload == this) { throw new IllegalArgumentException("Packet payload data could not be itself"); }

        payload.parentPacket(this);
        _payloadPacket = payload;
    }

    public abstract void accept(IPacketVisitor visitor);
}
