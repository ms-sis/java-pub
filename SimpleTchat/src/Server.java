import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    private static final int PORT = 1337;

    public static void main(String[] args){
        ServerSocket server = null;
        Socket lastConnection;

        try {
            server = new ServerSocket(PORT);
            System.out.println("Server listen on port > " + PORT);
            while (true) {
                lastConnection = server.accept();
                if (isServerNotFull()) {
                    new ServiceTchat(lastConnection);

                } else {
                    PrintStream out = new PrintStream(lastConnection.getOutputStream());
                    out.println("<SYSTEM> The server is full, please try later");
                    out.close();
                    lastConnection.close();
                }
            }
        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
        finally {
            try {
                server.close();
            } catch (Exception ignored) {}
        }
    }

    public synchronized static boolean isServerNotFull() {
        return !isServerFull();
    }

    public synchronized static boolean isServerFull() {
        return DataContext.LOGGED_IDENTITIES.size() >= DataContext.CLIENT_POOL_MAX_SIZE;
    }
}
