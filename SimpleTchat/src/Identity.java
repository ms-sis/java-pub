import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Identity {

    String _username;
    String _passwordHash;

    public Identity(String username, String password) {
        if (username == null || password == null) {
            throw new IllegalArgumentException("Invalid parameters, username|password should not be null");
        }

        _username = username;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(password.getBytes());
            _passwordHash = new String(md.digest());
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public String username() {
        return _username;
    }

    public boolean verifyPassword(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(password.getBytes());
            return _passwordHash.equals(new String(md.digest()));
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }
}
