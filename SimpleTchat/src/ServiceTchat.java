import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;

public class ServiceTchat extends Thread {

    private Identity _identity;
    private Socket _socket;
    private BufferedReader _in;
    private PrintStream _out;

    public ServiceTchat(Socket clientSocket) {
        try {
            _socket = clientSocket;
            _in = new BufferedReader(new InputStreamReader(_socket.getInputStream()));
            _out = new PrintStream(_socket.getOutputStream());

            if ((_identity = negotiateLogIn(_in, _out)) != null) {
                registerLoggedIdentityAndStart();
            }
        }
        catch (Exception e) {
            logOut();
        }
    }

    public static Identity negotiateLogIn(BufferedReader in, PrintStream out) {
        return negotiateLogIn(in, out, 5);
    }

    public static Identity negotiateLogIn(BufferedReader in, PrintStream out, int countAttemptBeforeLockout) {

        String username = null, password = null;
        boolean logInSucceeded;
        try {

            while( !(logInSucceeded = logIn(username, password, out)) && countAttemptBeforeLockout > 0 ) {
                --countAttemptBeforeLockout;

                out.println("<SYSTEM> What's your login ?");
                username = in.readLine();

                if (isIdentityLogged(username)) {
                    out.println("<SYSTEM> Unavailable login");
                    continue;
                }
                else {
                    out.println("<SYSTEM> What's your password");
                    password = in.readLine();
                }
            }

        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }

        if (logInSucceeded) {
            Identity loggedIdentity = findIdentityByUsername(username);
            if (loggedIdentity != null) {
                return loggedIdentity;
            } else {
                loggedIdentity = new Identity(username, password);
                registerIdentity(loggedIdentity);
                return loggedIdentity;
            }
        }
        return null;
    }

    private synchronized static boolean registerIdentity(Identity newIdentity) {
        return DataContext.REGISTERED_IDENTITIES.add(newIdentity);
    }

    private static boolean logIn(String username, String password, PrintStream out) {
        if (username == null || password == null) { return false; }

        Identity user;
        if ( ( user = findIdentityByUsername(username)) != null) {
            if (!user.verifyPassword(password)) {
                out.println("<SYSTEM> Username/password mismatch");
                return false;
            }
        }
        return true;
    }

    private synchronized static boolean isIdentityLogged(String username) {
        for(Identity identity : DataContext.LOGGED_IDENTITIES) {
            if (username.equalsIgnoreCase(identity.username())) {
                return true;
            }
        }
        return false;
    }

    private synchronized static Identity findIdentityByUsername(String username) {
        for (Identity identity : DataContext.REGISTERED_IDENTITIES) {
            if (username.equalsIgnoreCase(identity.username())) {
                return identity;
            }
        }
        return null;
    }

    private synchronized void registerLoggedIdentityAndStart() {
        if (Server.isServerNotFull()) {
            DataContext.LOGGED_IDENTITIES.add(_identity);
            DataContext.CLIENT_OUTPUT_STREAMS.add(_out);
            start();
        }
        else {
            _out.println("<SYSTEM> The server is full, please try later");
            throw new RuntimeException();
        }
    }

    @Override
    public void run() {
        try {
            ServiceTchat.broadcast("<SYSTEM> " + _identity.username() + " as join the channel");
            boolean listening = true;
            while(listening) {
                String msg = _in.readLine();
                String[] cmd = msg.split(" ", 3);
                switch (cmd[0]) {
                    case "/whois":
                        _out.println("<SYSTEM> Connected identities > ");
                        for(Identity identity : DataContext.LOGGED_IDENTITIES) {
                            _out.printf("\t - %s \n", identity.username());
                        }
                        break;
                    case "/msg":
                            if (cmd.length == 3) {
                                if (!sendPrivateMsg(cmd[1], cmd[2])) {
                                    _out.printf("<SYSTEM> User %s do not exist \n", cmd[2]);
                                }
                            }
                            else {
                                displayHelp();
                            }
                        break;
                    case "/help":
                        displayHelp();
                        break;
                    case "/exit":
                        listening = false;
                        break;
                    default:
                        if (!msg.equals("")) {
                            internalBroadcast("[" + _identity.username() + "] > " + msg);
                        }
                        break;
                }

            }
            logOut();
        }
        catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    private void displayHelp() {
        _out.println();
        _out.println("<SYSTEM> Available commands > ");
        _out.println("/whois         \t\t - List users connected to the current channel");
        _out.println("/msg [username]\t\t - Send a private message to [username]");
        _out.println("/help          \t\t - Display this help");
        _out.println("/exit          \t\t - Exit the channel");
        _out.println();
    }

    private synchronized boolean sendPrivateMsg(String to, String msg) {
        for(int i=0; i<DataContext.LOGGED_IDENTITIES.size(); ++i) {
            if (DataContext.LOGGED_IDENTITIES.get(i).username().equals(to)) {
                DataContext.CLIENT_OUTPUT_STREAMS.get(i).printf("[%s] (private) > %s \n", _identity.username(), msg);
                return true;
            }
        }
        return false;
    }

    private synchronized void internalBroadcast(String msg) {
        for(PrintStream client : DataContext.CLIENT_OUTPUT_STREAMS) {
            if(client != _out) {
                client.println(msg);
            }
        }
    }

    public static synchronized void broadcast(String msg) {
        for(PrintStream client : DataContext.CLIENT_OUTPUT_STREAMS) {
            client.println(msg);
        }
    }

    @Override
    public void finalize() throws Throwable {
        try {
            logOut();
            _out.close();
            _in.close();
        } catch (Exception ignored) {}
        finally {
            super.finalize();
        }
    }

    private synchronized void logOut() {
        internalBroadcast("<SYSTEM> " + _identity.username() + " has left the channel ");
        if (_out != null) {
            DataContext.CLIENT_OUTPUT_STREAMS.remove(_out);
            DataContext.LOGGED_IDENTITIES.remove(_identity);
        }
        try {
            _socket.close();
        } catch (Exception e) {}
    }
}
