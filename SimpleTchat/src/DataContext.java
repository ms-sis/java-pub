import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

public abstract class DataContext {
    public static List<Identity> REGISTERED_IDENTITIES = new ArrayList<>();
    public static List<Identity> LOGGED_IDENTITIES = new ArrayList<>();
    public static List<PrintStream> CLIENT_OUTPUT_STREAMS = new ArrayList<>();
    public static int CLIENT_POOL_MAX_SIZE = 42;

}
